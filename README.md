# ics-ans-role-vdct

Ansible role to install VisualDCT

## Role Variables

```yaml
---
vdct_version: 2.8.2
vdct_archive: "http://artifactory.esss.lu.se/artifactory/swi-pkg/VDCT/VisualDCT-{{ vdct_version }}-distribution.tar.gz"
vdct_epics_base_version: 7.0.3.1
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-vdct
```

## License

BSD 2-clause
