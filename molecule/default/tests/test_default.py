import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_dependencies_installed(host):
    which = host.package("which")
    assert which.is_installed


def test_vdct_environment(host):
    cmd = host.run("source /etc/profile && env")
    env = cmd.stdout
    assert "JAVA_HOME=/opt/java/jdk-11" in env
    assert "EPICS_DB_INCLUDE_PATH=/opt/conda/envs/vdct/base/dbd" in env


def test_vdct(host):
    cmd = host.run("cd /opt/VisualDCT && ./visualdct.sh --help")
    assert cmd.stdout.startswith("VisualDCT help")
